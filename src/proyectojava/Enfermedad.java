
package proyectojava;

import proyectojava.HistorialClinico.ConsultaMedica;

public class Enfermedad {
    
    int idEnfermedad;
    String nombre;

    HistorialClinico.ConsultaMedica[] consultasE;
    
    public Enfermedad(int idEnfermedad, String nombre) {
        this.idEnfermedad = idEnfermedad;
        this.nombre = nombre;
    }

    public ConsultaMedica[] getConsultasE() {
        return consultasE;
    }

    public void setConsultasE(ConsultaMedica[] consultasE) {
        this.consultasE = consultasE;
    }

    public int getIdEnfermedad() {
        return idEnfermedad;
    }

    public void setIdEnfermedad(int idEnfermedad) {
        this.idEnfermedad = idEnfermedad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
