package proyectojava;

import Ventanas.Datos;

public class Persona extends Datos{
    
    public String nombre;
    public String apellidos;
    public String ciudad;
    public String direccion;
    public String sexo;
    public int telefono;

    public Persona(String nombre, String apellidos, String ciudad, String direccion, String sexo, int telefono) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.sexo = sexo;
        this.telefono = telefono;
    }

}
