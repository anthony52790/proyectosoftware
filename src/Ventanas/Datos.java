package Ventanas;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import proyectojava.AlmacenMedicina;
import proyectojava.Calendario;
import proyectojava.Enfermedad;
import proyectojava.HistorialClinico;
import proyectojava.Medico;
import proyectojava.Paciente;

public class Datos extends javax.swing.JFrame {
    
    DefaultTableModel tabla = new DefaultTableModel();
    DefaultTableModel tabla1 = new DefaultTableModel();
    DefaultTableModel tabla2 = new DefaultTableModel();
    
    ArrayList paciente = new ArrayList();
    ArrayList medico = new ArrayList();
    ArrayList medicina = new ArrayList();
    ArrayList enfermedad = new ArrayList();
    ArrayList calendario = new ArrayList();
    ArrayList Historial = new ArrayList();
    
    ArrayList codigos1 = new ArrayList();
    ArrayList codigos2 = new ArrayList();
    ArrayList codigos3 = new ArrayList();
    ArrayList codigos4 = new ArrayList();
    ArrayList codigos5 = new ArrayList();
    
    Paciente pa;
    Medico me;
    Enfermedad en;
    
    Calendario ca;
    Calendario.CitaMedica cm;
    
    AlmacenMedicina m;
    AlmacenMedicina.Medicina am;
    
    HistorialClinico h;
    HistorialClinico.ConsultaMedica hc;

    @Override
    public Image getIconImage(){
    Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("Imagenes/monitor.png"));
    return retValue;
    }
    
    public void tabla1(){
    String titulos[] = {"Codigo/Medicina","Codigo/Enfermedad","Codigo/Consulta","Cita/Fecha"};
    tabla1.setColumnIdentifiers(titulos);
    this.txtTabla4.setModel(tabla1);
    }
    public void tabla2(){
    String titulos[]={"Codigo/Cita","Fecha","Codigo/Paciente","Codigo/Medico"};
    tabla2.setColumnIdentifiers(titulos);
    this.txtTabla5.setModel(tabla2);
    }

    public Datos() {
        initComponents();
        jtprincipal.setVisible(false);
        marcar();
        String titulos[] = {"Codigo","Nombre","FechaVen"};
        tabla.setColumnIdentifiers(titulos);
        this.txttabla3.setModel(tabla);
        tabla1();
        tabla2();
    }
    public void marcar(){

       jPanel6.setVisible(false);
    }
    public void limpiarPaciente(){
       txtnombre1.setText("");
       txtapellido1.setText("");
       txtdireccion1.setText("");
       txtciudad1.setText("");
       txtsexo1.setText("");
       txttel1.setText("");
       txtcodigo1.setText("");
       txtsocial1.setText("");
       txtfecha1.setText("");
    }
    public void limpiarMedicina(){
       txtcodigoMedicina.setText("");
       txtNombreMedicina.setText("");
       txtFechaMedicina.setText("");
    }
    public void limpiarDoctor(){
       txtnombre.setText("");
       txtapellido.setText("");
       txtdireccion.setText("");
       txtciudad.setText("");
       txtsexo.setText("");
       txtTelefono.setText("");
       txtidmedico.setText("");
       txtcedula.setText("");
       txtespecialidad.setText("");
    }
    public void limiparCita(){
        txtCodigoCitaI.setText("");
        txtCodigoFechaI.setText("");
        txtCodigoPacientI.setText("");
        txtCodigoMedicoI.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jtprincipal = new javax.swing.JTabbedPane();
        cmdVipaciente = new javax.swing.JInternalFrame();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtnombre1 = new javax.swing.JTextField();
        txtapellido1 = new javax.swing.JTextField();
        txtdireccion1 = new javax.swing.JTextField();
        txtciudad1 = new javax.swing.JTextField();
        txtsexo1 = new javax.swing.JTextField();
        txttel1 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtcodigo1 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtsocial1 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtfecha1 = new javax.swing.JTextField();
        cmdGuardarP = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        cmdBuscar = new javax.swing.JButton();
        txtbusquedad = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        cmdLimpiar = new javax.swing.JButton();
        cmdVimedico = new javax.swing.JInternalFrame();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        txtapellido = new javax.swing.JTextField();
        txtdireccion = new javax.swing.JTextField();
        txtciudad = new javax.swing.JTextField();
        txtsexo = new javax.swing.JTextField();
        txtTelefono = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtidmedico = new javax.swing.JTextField();
        txtcedula = new javax.swing.JTextField();
        txtespecialidad = new javax.swing.JTextField();
        cmdGuardar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        txtbusca1 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        cmdBuscarMedico = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        cmdLimipar2 = new javax.swing.JButton();
        jInternalFrame3 = new javax.swing.JInternalFrame();
        jPanel7 = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();
        txtcodigoMedicina = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        txtNombreMedicina = new javax.swing.JTextField();
        txtFechaMedicina = new javax.swing.JTextField();
        cmdGuardarMedicina = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txttabla3 = new javax.swing.JTable();
        txtCodigoMedi = new javax.swing.JTextField();
        cmdBuscarMedicina = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        cmdLimpiarDatoA = new javax.swing.JButton();
        jInternalFrame2 = new javax.swing.JInternalFrame();
        cmdGuardarEnfermedad = new javax.swing.JButton();
        cmdBuscarEnfermedad = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        txtCodigoEnfermedad = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        txtNombreEnfermedad = new javax.swing.JTextField();
        txtConsultaEnfermedad = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        jInternalFrame4 = new javax.swing.JInternalFrame();
        jPanel11 = new javax.swing.JPanel();
        jLabel45 = new javax.swing.JLabel();
        txtCodigoCitaI = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        txtCodigoFechaI = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        txtCodigoPacientI = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        txtCodigoMedicoI = new javax.swing.JTextField();
        cmdInforme = new javax.swing.JButton();
        cmdGuardarCita = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtTabla5 = new javax.swing.JTable();
        jInternalFrame1 = new javax.swing.JInternalFrame();
        jPanel8 = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        txtNombreMedicinaH = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        txtNombreEnfermedadH = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        txtFechaH = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtCampo = new javax.swing.JTextArea();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        txtCodigoH = new javax.swing.JTextField();
        cmdBuscarHistorial = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();
        txtBusquedadH = new javax.swing.JTextField();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtTabla4 = new javax.swing.JTable();
        cmdGuardarHistorial = new javax.swing.JButton();
        cmdEliminarHistorial = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setIconImage(getIconImage());
        setLocationByPlatform(true);
        setMaximizedBounds(new java.awt.Rectangle(0, 0, 600, 600));

        jtprincipal.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informacion Paciente", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel5.setText("Telefono");

        jLabel6.setText("Sexo");

        jLabel4.setText("Ciudad");

        jLabel3.setText("Direccion");

        jLabel2.setText("Apellidos");

        jLabel1.setText("Nombre");

        txtnombre1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtnombre1MouseClicked(evt);
            }
        });

        txtapellido1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtapellido1MouseClicked(evt);
            }
        });

        txtdireccion1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtdireccion1MouseClicked(evt);
            }
        });

        txtciudad1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtciudad1MouseClicked(evt);
            }
        });

        txtsexo1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtsexo1MouseClicked(evt);
            }
        });

        txttel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txttel1MouseClicked(evt);
            }
        });

        jLabel19.setText("Codigo");

        txtcodigo1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtcodigo1MouseClicked(evt);
            }
        });

        jLabel20.setText("Nro regSocial");

        txtsocial1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtsocial1MouseClicked(evt);
            }
        });

        jLabel21.setText("Fnacimiento");

        txtfecha1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtfecha1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtnombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtapellido1, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtciudad1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                                .addComponent(txtdireccion1, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtsexo1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                                .addComponent(txttel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE))))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel20)
                                .addComponent(jLabel19))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtcodigo1)
                                .addComponent(txtfecha1)
                                .addComponent(txtsocial1, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)))
                        .addComponent(jLabel21)))
                .addContainerGap(67, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtnombre1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtapellido1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtdireccion1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtciudad1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtsexo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txttel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(55, 55, 55)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel19)
                    .addComponent(txtcodigo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(txtsocial1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(txtfecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        cmdGuardarP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save_16.png"))); // NOI18N
        cmdGuardarP.setText("Guardar Paciente");
        cmdGuardarP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdGuardarPActionPerformed(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Consulta Paciente", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        cmdBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_16.png"))); // NOI18N
        cmdBuscar.setText("Consulta");
        cmdBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdBuscarActionPerformed(evt);
            }
        });

        txtbusquedad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbusquedadActionPerformed(evt);
            }
        });

        jLabel28.setText("Nombre / Codigo");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(txtbusquedad, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jLabel28))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addComponent(cmdBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cmdBuscar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel28)
                .addGap(18, 18, 18)
                .addComponent(txtbusquedad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBackground(new java.awt.Color(51, 204, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/user_64.png"))); // NOI18N

        jLabel30.setText("Informacion Correcta");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(jLabel30)
                .addContainerGap())
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel29)
                .addContainerGap(32, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel29)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel30)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cmdLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/delete_16.png"))); // NOI18N
        cmdLimpiar.setText("Limpiar");
        cmdLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdLimpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout cmdVipacienteLayout = new javax.swing.GroupLayout(cmdVipaciente.getContentPane());
        cmdVipaciente.getContentPane().setLayout(cmdVipacienteLayout);
        cmdVipacienteLayout.setHorizontalGroup(
            cmdVipacienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cmdVipacienteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(cmdVipacienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cmdVipacienteLayout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(19, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cmdVipacienteLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(cmdVipacienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cmdVipacienteLayout.createSequentialGroup()
                                .addComponent(cmdLimpiar)
                                .addGap(75, 75, 75))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cmdVipacienteLayout.createSequentialGroup()
                                .addComponent(cmdGuardarP)
                                .addGap(37, 37, 37))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cmdVipacienteLayout.createSequentialGroup()
                                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(50, 50, 50))))))
        );
        cmdVipacienteLayout.setVerticalGroup(
            cmdVipacienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cmdVipacienteLayout.createSequentialGroup()
                .addGroup(cmdVipacienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(cmdVipacienteLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(cmdVipacienteLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(cmdGuardarP)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cmdLimpiar)))
                .addContainerGap(42, Short.MAX_VALUE))
        );

        jtprincipal.addTab("Paciente", cmdVipaciente);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informacion Personal", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel9.setText("Nombre");

        jLabel10.setText("Apellidos");

        jLabel11.setText("Direccion");

        jLabel12.setText("Ciudad");

        jLabel13.setText("Sexo");

        jLabel14.setText("Telefono");

        txtTelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTelefonoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel10)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14))))
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtTelefono)
                    .addComponent(txtsexo)
                    .addComponent(txtciudad)
                    .addComponent(txtdireccion)
                    .addComponent(txtnombre, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
                    .addComponent(txtapellido))
                .addGap(26, 26, 26))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtapellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(txtciudad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(txtsexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informacion Profesional", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel16.setText("Especialidad");

        jLabel7.setText("Nro Cedula");

        jLabel15.setText("Codigo");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16))
                .addGap(40, 40, 40)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtespecialidad)
                    .addComponent(txtcedula, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtidmedico, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(jLabel7))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtidmedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15))
                        .addGap(18, 18, 18)
                        .addComponent(txtcedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtespecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16))))
                .addContainerGap(44, Short.MAX_VALUE))
        );

        cmdGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save_16.png"))); // NOI18N
        cmdGuardar.setText("Guardar informacion");
        cmdGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdGuardarActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Busquedad de Medico", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel8.setText("Nombre / Codigo");

        cmdBuscarMedico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_16.png"))); // NOI18N
        cmdBuscarMedico.setText("Buscar medico");
        cmdBuscarMedico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdBuscarMedicoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmdBuscarMedico, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
                            .addComponent(txtbusca1, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(47, 47, 47))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cmdBuscarMedico)
                .addGap(18, 18, 18)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(txtbusca1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70))
        );

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/monitor_64.png"))); // NOI18N
        jLabel18.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel18.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        cmdLimipar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/delete_16.png"))); // NOI18N
        cmdLimipar2.setText("Limpiar");
        cmdLimipar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdLimipar2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout cmdVimedicoLayout = new javax.swing.GroupLayout(cmdVimedico.getContentPane());
        cmdVimedico.getContentPane().setLayout(cmdVimedicoLayout);
        cmdVimedicoLayout.setHorizontalGroup(
            cmdVimedicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cmdVimedicoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cmdVimedicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(cmdVimedicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cmdVimedicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(cmdVimedicoLayout.createSequentialGroup()
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addContainerGap())
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cmdVimedicoLayout.createSequentialGroup()
                            .addComponent(jLabel18)
                            .addGap(69, 69, 69))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cmdVimedicoLayout.createSequentialGroup()
                            .addComponent(cmdLimipar2, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(60, 60, 60)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cmdVimedicoLayout.createSequentialGroup()
                        .addComponent(cmdGuardar)
                        .addGap(32, 32, 32))))
        );
        cmdVimedicoLayout.setVerticalGroup(
            cmdVimedicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cmdVimedicoLayout.createSequentialGroup()
                .addGroup(cmdVimedicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cmdVimedicoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(cmdVimedicoLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(cmdGuardar)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cmdLimipar2)))
                .addContainerGap())
        );

        jtprincipal.addTab("Medico", cmdVimedico);

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Medicina", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel35.setText("Codigo");

        jLabel36.setText("Nombre");

        jLabel37.setText("Fvencimiento");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel37, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel36, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtFechaMedicina)
                    .addComponent(txtNombreMedicina)
                    .addComponent(txtcodigoMedicina, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcodigoMedicina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel36)
                    .addComponent(txtNombreMedicina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel37)
                    .addComponent(txtFechaMedicina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cmdGuardarMedicina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save_16.png"))); // NOI18N
        cmdGuardarMedicina.setText("Guardar Medicina");
        cmdGuardarMedicina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdGuardarMedicinaActionPerformed(evt);
            }
        });

        txttabla3.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        txttabla3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Nombre", "FechaVen"
            }
        ));
        jScrollPane1.setViewportView(txttabla3);

        cmdBuscarMedicina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_16.png"))); // NOI18N
        cmdBuscarMedicina.setText("Buscar Medicina");
        cmdBuscarMedicina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdBuscarMedicinaActionPerformed(evt);
            }
        });

        jLabel17.setText("Codigo");

        jLabel49.setBackground(new java.awt.Color(0, 255, 255));
        jLabel49.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/medicina.png"))); // NOI18N

        cmdLimpiarDatoA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/delete_16.png"))); // NOI18N
        cmdLimpiarDatoA.setText("Limpiar");
        cmdLimpiarDatoA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdLimpiarDatoAActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jInternalFrame3Layout = new javax.swing.GroupLayout(jInternalFrame3.getContentPane());
        jInternalFrame3.getContentPane().setLayout(jInternalFrame3Layout);
        jInternalFrame3Layout.setHorizontalGroup(
            jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrame3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jInternalFrame3Layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmdGuardarMedicina, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmdBuscarMedicina, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                .addGap(49, 49, 49)
                                .addComponent(jLabel17))
                            .addComponent(txtCodigoMedi, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(cmdLimpiarDatoA))
                            .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel49)))
                        .addGap(21, 21, 21))
                    .addGroup(jInternalFrame3Layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())))
        );
        jInternalFrame3Layout.setVerticalGroup(
            jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrame3Layout.createSequentialGroup()
                .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jInternalFrame3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jInternalFrame3Layout.createSequentialGroup()
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(cmdGuardarMedicina)
                                .addGap(18, 18, 18)
                                .addComponent(cmdBuscarMedicina)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel17))
                            .addGroup(jInternalFrame3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jInternalFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCodigoMedi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmdLimpiarDatoA))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)
                .addContainerGap())
        );

        jtprincipal.addTab("Almacen/Medicina", jInternalFrame3);

        cmdGuardarEnfermedad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save_16.png"))); // NOI18N
        cmdGuardarEnfermedad.setText("GuardarEnfermedad");
        cmdGuardarEnfermedad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdGuardarEnfermedadActionPerformed(evt);
            }
        });

        cmdBuscarEnfermedad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_16.png"))); // NOI18N
        cmdBuscarEnfermedad.setText("Consultar Enfermedad");
        cmdBuscarEnfermedad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdBuscarEnfermedadActionPerformed(evt);
            }
        });

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Enfermedad", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel39.setText("Codigo");

        jLabel38.setText("Nombre");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel39, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel38, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(14, 14, 14)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(txtCodigoEnfermedad, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                        .addContainerGap(20, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel10Layout.createSequentialGroup()
                        .addComponent(txtNombreEnfermedad, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(txtCodigoEnfermedad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(txtNombreEnfermedad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel50.setText("Nombre");

        javax.swing.GroupLayout jInternalFrame2Layout = new javax.swing.GroupLayout(jInternalFrame2.getContentPane());
        jInternalFrame2.getContentPane().setLayout(jInternalFrame2Layout);
        jInternalFrame2Layout.setHorizontalGroup(
            jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrame2Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtConsultaEnfermedad)
                    .addComponent(cmdBuscarEnfermedad, 0, 0, Short.MAX_VALUE)
                    .addComponent(cmdGuardarEnfermedad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jLabel50)
                .addGap(74, 74, 74))
        );
        jInternalFrame2Layout.setVerticalGroup(
            jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrame2Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jInternalFrame2Layout.createSequentialGroup()
                        .addComponent(cmdGuardarEnfermedad)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmdBuscarEnfermedad)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtConsultaEnfermedad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel50)))
                    .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(282, Short.MAX_VALUE))
        );

        jtprincipal.addTab("Enfermedad", jInternalFrame2);

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cita Medica", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel45.setText("Codigo/Cita");

        jLabel46.setText("Fecha");

        jLabel47.setText("Codigo/Paciente");

        jLabel48.setText("Codigo/Medico");

        cmdInforme.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/briefcase_16.png"))); // NOI18N
        cmdInforme.setText("Hacer Cita");
        cmdInforme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdInformeActionPerformed(evt);
            }
        });

        cmdGuardarCita.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save_16.png"))); // NOI18N
        cmdGuardarCita.setText("Guardar Cita");
        cmdGuardarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdGuardarCitaActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/delete_16.png"))); // NOI18N
        jButton1.setText("Limpiar");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_16.png"))); // NOI18N
        jButton2.setText("Buscar Cita");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel45)
                            .addComponent(jLabel46))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCodigoFechaI)
                            .addComponent(txtCodigoCitaI, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(cmdGuardarCita)
                        .addGap(35, 35, 35)
                        .addComponent(jButton2)))
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel47)
                            .addComponent(jLabel48))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCodigoMedicoI)
                            .addComponent(txtCodigoPacientI, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(cmdInforme)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel47)
                            .addComponent(txtCodigoPacientI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel48)
                            .addComponent(txtCodigoMedicoI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel45)
                            .addComponent(txtCodigoCitaI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel46)
                            .addComponent(txtCodigoFechaI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(cmdGuardarCita)
                    .addComponent(jButton2)
                    .addComponent(cmdInforme))
                .addContainerGap())
        );

        txtTabla5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo/Cita", "Fecha", "Codigo/Paciente", "Codigo/Medico"
            }
        ));
        jScrollPane4.setViewportView(txtTabla5);

        javax.swing.GroupLayout jInternalFrame4Layout = new javax.swing.GroupLayout(jInternalFrame4.getContentPane());
        jInternalFrame4.getContentPane().setLayout(jInternalFrame4Layout);
        jInternalFrame4Layout.setHorizontalGroup(
            jInternalFrame4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInternalFrame4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jInternalFrame4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 563, Short.MAX_VALUE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jInternalFrame4Layout.setVerticalGroup(
            jInternalFrame4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrame4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                .addContainerGap())
        );

        jtprincipal.addTab("Calendario", jInternalFrame4);

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Consulta Medica", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel40.setText("Nombre/Medicina");

        jLabel41.setText("Nombre/Enfermedad");

        jLabel42.setText("Cita/fecha");

        txtCampo.setColumns(20);
        txtCampo.setRows(5);
        jScrollPane2.setViewportView(txtCampo);

        jLabel43.setText("Diagnostico");

        jLabel44.setText("Codigo/Consulta");

        cmdBuscarHistorial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_16.png"))); // NOI18N
        cmdBuscarHistorial.setText("Buscar Historial");
        cmdBuscarHistorial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdBuscarHistorialActionPerformed(evt);
            }
        });

        jLabel31.setText("Nombre del paciente");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel40)
                            .addComponent(jLabel41, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNombreEnfermedadH)
                            .addComponent(txtNombreMedicinaH, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                        .addGap(41, 41, 41)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel42, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel44, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtFechaH, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                            .addComponent(txtCodigoH, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel43)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGap(84, 84, 84)
                                .addComponent(cmdBuscarHistorial)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel31)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtBusquedadH, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel40)
                    .addComponent(txtNombreMedicinaH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel42)
                    .addComponent(txtFechaH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel44)
                        .addComponent(txtCodigoH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtNombreEnfermedadH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel41)))
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel8Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jLabel43))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmdBuscarHistorial)
                            .addComponent(jLabel31)
                            .addComponent(txtBusquedadH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Historial", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        txtTabla4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo/Medicina", "Codigo/Enfermedad", "Codigo/Consulta", "Cita/Fecha"
            }
        ));
        jScrollPane3.setViewportView(txtTabla4);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 541, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cmdGuardarHistorial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save_16.png"))); // NOI18N
        cmdGuardarHistorial.setText("Guardar Informe");
        cmdGuardarHistorial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdGuardarHistorialActionPerformed(evt);
            }
        });

        cmdEliminarHistorial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/delete_16.png"))); // NOI18N
        cmdEliminarHistorial.setText("Eliminar Informe");

        javax.swing.GroupLayout jInternalFrame1Layout = new javax.swing.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrame1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jInternalFrame1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(cmdGuardarHistorial)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 291, Short.MAX_VALUE)
                        .addComponent(cmdEliminarHistorial))
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jInternalFrame1Layout.setVerticalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrame1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmdGuardarHistorial)
                    .addComponent(cmdEliminarHistorial))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jtprincipal.addTab("Historial", jInternalFrame1);

        jMenu1.setText("Archivo");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu1MouseClicked(evt);
            }
        });
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hard_disk.png"))); // NOI18N
        jMenuItem1.setText("Nueva Informacion");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);
        jMenu1.add(jSeparator1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/block_16.png"))); // NOI18N
        jMenuItem2.setText("Salir");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Acerca ...");

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/help_16.png"))); // NOI18N
        jMenuItem3.setText("Acerca");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtprincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 604, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtprincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseClicked
    
    }//GEN-LAST:event_jMenu1MouseClicked

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        jtprincipal.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
       
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        Derechos d = new Derechos();
        d.setVisible(true);
        d.setResizable(false);
        d.setMaximizedBounds(null);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void txtTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTelefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTelefonoActionPerformed

    private void cmdBuscarMedicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdBuscarMedicoActionPerformed
   
       int tamanio = medico.size();
       boolean encontro = false;
       
       Medico aux = null;
       int i=0;
       while(!encontro && i<tamanio){
            aux=(Medico)medico.get(i);
            if(txtbusca1.getText().equals(aux.getNombre())||Integer.valueOf(txtbusca1.getText()).equals(aux.getIdMedico())){
              encontro=true;
            }
            i++;
       }
       if(encontro){
         txtnombre.setText(String.valueOf(aux.getNombre()));
         txtapellido.setText(String.valueOf(aux.getApellidos()));
         txtdireccion.setText(String.valueOf(aux.getDireccion()));
         txtciudad.setText(String.valueOf(aux.getCiudad()));
         txtsexo.setText(String.valueOf(aux.getSexo()));
         txtTelefono.setText(String.valueOf(aux.getTelefono()));
         txtidmedico.setText(String.valueOf(aux.getIdMedico()));
         txtcedula.setText(String.valueOf(aux.getNroCedula())); 
         txtespecialidad.setText(String.valueOf(aux.getEspecialidad()));
       }
       else{
           JOptionPane.showMessageDialog(this,"Datos no encontrados","[X]Informe",JOptionPane.ERROR_MESSAGE);
        } 
    }//GEN-LAST:event_cmdBuscarMedicoActionPerformed

    private void cmdGuardarPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdGuardarPActionPerformed

       pa = new Paciente(Integer.parseInt(txtcodigo1.getText()),Integer.parseInt(txtsocial1.getText()),txtfecha1.getText(),txtnombre1.getText(),txtapellido1.getText(),txtciudad1.getText(),txtdireccion1.getText(),txtsexo1.getText(),Integer.parseInt(txttel1.getText()));
       paciente.add(pa);
       codigos1.add(pa);
       jPanel6.setVisible(true);
       limpiarPaciente();
       
    }//GEN-LAST:event_cmdGuardarPActionPerformed

    private void txtbusquedadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbusquedadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbusquedadActionPerformed

    private void cmdGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdGuardarActionPerformed

        me = new Medico(Integer.parseInt(txtidmedico.getText()), Integer.parseInt(txtcedula.getText()),txtespecialidad.getText(),txtnombre.getText(),txtapellido.getText(),txtciudad.getText(),txtdireccion.getText(),txtsexo.getText(), Integer.parseInt(txtTelefono.getText()));
        medico.add(me);
        codigos2.add(me);
        limpiarDoctor();
    }//GEN-LAST:event_cmdGuardarActionPerformed

    private void cmdBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdBuscarActionPerformed
        int tamanio = paciente.size();
        boolean encontrado = false;
        
        Paciente aux = null;
        int i=0;
        while(!encontrado && i<tamanio){
            aux=(Paciente)paciente.get(i);
            if(txtbusquedad.getText().equals(aux.getNombre())||Integer.valueOf(txtbusquedad.getText()).equals(aux.getIdPaciente())){
               encontrado = true;
            }
            i++;
        }
        if(encontrado){
           txtnombre1.setText(String.valueOf(aux.getNombre()));
           txtapellido1.setText(String.valueOf(aux.getApellidos()));
           txtdireccion1.setText(String.valueOf(aux.getDireccion()));
           txtciudad1.setText(String.valueOf(aux.getCiudad()));
           txtsexo1.setText(String.valueOf(aux.getSexo()));
           txttel1.setText(String.valueOf(aux.getTelefono()));
           txtcodigo1.setText(String.valueOf(aux.getIdPaciente()));
           txtsocial1.setText(String.valueOf(aux.getNroRegistro()));
           txtfecha1.setText(String.valueOf(aux.getfNacimiento()));
        }
        else{
           JOptionPane.showMessageDialog(this,"Datos no encontrados","[X]Informe",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_cmdBuscarActionPerformed

    private void cmdLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdLimpiarActionPerformed
        limpiarPaciente();
    }//GEN-LAST:event_cmdLimpiarActionPerformed

    private void cmdLimipar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdLimipar2ActionPerformed
        limpiarDoctor();
    }//GEN-LAST:event_cmdLimipar2ActionPerformed

    private void cmdBuscarHistorialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdBuscarHistorialActionPerformed
      int tamano = codigos1.size();
      int tamanio1 = codigos3.size();
      int tamanio2 = codigos4.size();
      int tamanio3 = codigos5.size();
      boolean encontro = false;
      Paciente aux = null;
      AlmacenMedicina.Medicina aux1 = null;
      Enfermedad aux2 = null;
      Calendario.CitaMedica aux3 = null;
      int i=0,j=0,k=0,a=0;
      while((!encontro && a<tamano)&&(!encontro && i<tamanio1)&&(!encontro && j<tamanio2)&&(!encontro && k<tamanio3)){      
          aux=(Paciente)codigos1.get(a);
          aux1=(AlmacenMedicina.Medicina)codigos3.get(i);
          aux2=(Enfermedad)codigos4.get(j);
          aux3=(Calendario.CitaMedica)codigos5.get(k);
          if(Integer.valueOf(txtNombreMedicinaH.getText()).equals(aux1.getIdMedicina())&& Integer.valueOf(txtNombreEnfermedadH.getText()).equals(aux2.getIdEnfermedad()) && Integer.valueOf(txtFechaH.getText()).equals(aux3.getIdCita())){
           encontro = true;
          }
          a++;
          i++;
          j++;
          k++;
      }
      if(encontro){
             JOptionPane.showMessageDialog(this,"Datos encontrados","mensaje",JOptionPane.INFORMATION_MESSAGE);
      }
      else{
         JOptionPane.showMessageDialog(this,"Datos no encontrados","[X]Informe",JOptionPane.ERROR_MESSAGE);
      }
    }//GEN-LAST:event_cmdBuscarHistorialActionPerformed

    private void cmdGuardarMedicinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdGuardarMedicinaActionPerformed

      m = new AlmacenMedicina(100); 
      am = m.new Medicina(Integer.parseInt(txtcodigoMedicina.getText()),txtNombreMedicina.getText(),txtFechaMedicina.getText());
      codigos5.add(am);
      medicina.add(am);     
      String datos[] = new String[3];
          datos[0] = new Integer(this.txtcodigoMedicina.getText()).toString();
          datos[1] = this.txtNombreMedicina.getText();
          datos[2] = this.txtFechaMedicina.getText();
      tabla.addRow(datos);
      limpiarMedicina(); 
    }//GEN-LAST:event_cmdGuardarMedicinaActionPerformed

    private void cmdBuscarMedicinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdBuscarMedicinaActionPerformed
       int tamanio = medicina.size();
       boolean encontrado = false;
       
       AlmacenMedicina.Medicina aux = null;
       int i=0;
       while(!encontrado && i<tamanio){
          aux=(AlmacenMedicina.Medicina)medicina.get(i);
          if(Integer.valueOf(txtCodigoMedi.getText()).equals(aux.getIdMedicina())){
              encontrado = true;
          }
          i++;
       }
       if(encontrado){
           txtcodigoMedicina.setText(String.valueOf(aux.getIdMedicina()));
           txtNombreMedicina.setText(String.valueOf(aux.getNombre()));
           txtFechaMedicina.setText(String.valueOf(aux.getfVencimiento()));
       }
       else{
         JOptionPane.showMessageDialog(this,"Datos no Encontrados","[X]Informe",JOptionPane.ERROR_MESSAGE);
       }
    }//GEN-LAST:event_cmdBuscarMedicinaActionPerformed

    private void cmdLimpiarDatoAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdLimpiarDatoAActionPerformed
        limpiarMedicina();
    }//GEN-LAST:event_cmdLimpiarDatoAActionPerformed

    private void cmdGuardarEnfermedadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdGuardarEnfermedadActionPerformed
        en = new Enfermedad(Integer.parseInt(txtCodigoEnfermedad.getText()),txtNombreEnfermedad.getText());
        codigos4.add(en);
        enfermedad.add(en);
        txtCodigoEnfermedad.setText("");
        txtNombreEnfermedad.setText("");
    }//GEN-LAST:event_cmdGuardarEnfermedadActionPerformed

    private void cmdBuscarEnfermedadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdBuscarEnfermedadActionPerformed
        int tamanio = enfermedad.size();
        boolean encontrado = false;
        
        Enfermedad aux = null;
        int i=0;
        while(!encontrado && i<tamanio){
          aux=(Enfermedad)enfermedad.get(i);
          if(txtConsultaEnfermedad.getText().equals(aux.getNombre())){
            encontrado = true;
          }
          i++;
        }
        if(encontrado){
          txtCodigoEnfermedad.setText(String.valueOf(aux.getIdEnfermedad()));
          txtNombreEnfermedad.setText(String.valueOf(aux.getNombre()));
        }
        else{
          JOptionPane.showMessageDialog(this,"Datos no Encontrados","[X]Informe",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_cmdBuscarEnfermedadActionPerformed

    private void cmdGuardarHistorialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdGuardarHistorialActionPerformed
     /* h = new HistorialClinico();
      hc = h.new ConsultaMedica(Integer.parseInt(txtCodigoH.getText()),txtCampo.getText());
      //calendario.add(hc);
      Historial.add(hc);
      String datos[] = 
            datos[0] = 
            datos[1] = 
            datos[2] = 
            datos[3] = 
      tabla1.addRow(datos);*/
    }//GEN-LAST:event_cmdGuardarHistorialActionPerformed

    private void cmdInformeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdInformeActionPerformed
        
       int tamanio = codigos1.size();
       int tamanio1 = codigos2.size();
       boolean encontro = false;
       Paciente aux1 = null;
       Medico aux2 = null;
       int i=0;
       int j=0;
       while((!encontro && i<tamanio) && (!encontro && j<tamanio1)){
          aux1=(Paciente)codigos1.get(i);
          aux2=(Medico)codigos2.get(j);
          if(Integer.valueOf(txtCodigoPacientI.getText()).equals(aux1.getIdPaciente()) && Integer.valueOf(txtCodigoMedicoI.getText()).equals(aux2.getIdMedico())){
          encontro = true;
          }
          i++;
          j++;
       }
       if(encontro){
         JOptionPane.showMessageDialog(this,"Datos Encontrados\nHacer Cita","Mensaje",JOptionPane.INFORMATION_MESSAGE);
       }
       else{
         JOptionPane.showMessageDialog(this,"Datos no Encontrados","[X]Informe",JOptionPane.ERROR_MESSAGE);
       }
    }//GEN-LAST:event_cmdInformeActionPerformed

    private void cmdGuardarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdGuardarCitaActionPerformed
        ca = new Calendario(100);
        cm = ca.new CitaMedica(Integer.parseInt(txtCodigoCitaI.getText()),txtCodigoFechaI.getText());
        codigos3.add(cm);
        calendario.add(cm);
        
        String Datos[] = new String[4];
            Datos[0] = new Integer(this.txtCodigoCitaI.getText()).toString();
            Datos[1] = this.txtCodigoFechaI.getText();
            Datos[2] = new Integer(this.txtCodigoPacientI.getText()).toString();
            Datos[3] = new Integer(this.txtCodigoMedicoI.getText()).toString();
        tabla2.addRow(Datos);            
        limiparCita();
    }//GEN-LAST:event_cmdGuardarCitaActionPerformed

    private void txtfecha1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtfecha1MouseClicked
   
    }//GEN-LAST:event_txtfecha1MouseClicked

    private void txtsocial1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtsocial1MouseClicked

    }//GEN-LAST:event_txtsocial1MouseClicked

    private void txtcodigo1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtcodigo1MouseClicked

    }//GEN-LAST:event_txtcodigo1MouseClicked

    private void txttel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txttel1MouseClicked
 
    }//GEN-LAST:event_txttel1MouseClicked

    private void txtsexo1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtsexo1MouseClicked

    }//GEN-LAST:event_txtsexo1MouseClicked

    private void txtciudad1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtciudad1MouseClicked
        
    }//GEN-LAST:event_txtciudad1MouseClicked

    private void txtdireccion1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtdireccion1MouseClicked
      
    }//GEN-LAST:event_txtdireccion1MouseClicked

    private void txtapellido1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtapellido1MouseClicked
   
    }//GEN-LAST:event_txtapellido1MouseClicked

    private void txtnombre1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtnombre1MouseClicked

        jPanel6.setVisible(false);
    }//GEN-LAST:event_txtnombre1MouseClicked
        
    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Datos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Datos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Datos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Datos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new Datos().setVisible(true);               
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdBuscar;
    private javax.swing.JButton cmdBuscarEnfermedad;
    private javax.swing.JButton cmdBuscarHistorial;
    private javax.swing.JButton cmdBuscarMedicina;
    private javax.swing.JButton cmdBuscarMedico;
    private javax.swing.JButton cmdEliminarHistorial;
    private javax.swing.JButton cmdGuardar;
    private javax.swing.JButton cmdGuardarCita;
    private javax.swing.JButton cmdGuardarEnfermedad;
    private javax.swing.JButton cmdGuardarHistorial;
    private javax.swing.JButton cmdGuardarMedicina;
    private javax.swing.JButton cmdGuardarP;
    private javax.swing.JButton cmdInforme;
    private javax.swing.JButton cmdLimipar2;
    private javax.swing.JButton cmdLimpiar;
    private javax.swing.JButton cmdLimpiarDatoA;
    private javax.swing.JInternalFrame cmdVimedico;
    private javax.swing.JInternalFrame cmdVipaciente;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JInternalFrame jInternalFrame2;
    private javax.swing.JInternalFrame jInternalFrame3;
    private javax.swing.JInternalFrame jInternalFrame4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTabbedPane jtprincipal;
    private javax.swing.JTextField txtBusquedadH;
    private javax.swing.JTextArea txtCampo;
    private javax.swing.JTextField txtCodigoCitaI;
    private javax.swing.JTextField txtCodigoEnfermedad;
    private javax.swing.JTextField txtCodigoFechaI;
    private javax.swing.JTextField txtCodigoH;
    private javax.swing.JTextField txtCodigoMedi;
    private javax.swing.JTextField txtCodigoMedicoI;
    private javax.swing.JTextField txtCodigoPacientI;
    private javax.swing.JTextField txtConsultaEnfermedad;
    private javax.swing.JTextField txtFechaH;
    private javax.swing.JTextField txtFechaMedicina;
    private javax.swing.JTextField txtNombreEnfermedad;
    private javax.swing.JTextField txtNombreEnfermedadH;
    private javax.swing.JTextField txtNombreMedicina;
    private javax.swing.JTextField txtNombreMedicinaH;
    private javax.swing.JTable txtTabla4;
    private javax.swing.JTable txtTabla5;
    private javax.swing.JTextField txtTelefono;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JTextField txtapellido1;
    private javax.swing.JTextField txtbusca1;
    private javax.swing.JTextField txtbusquedad;
    private javax.swing.JTextField txtcedula;
    private javax.swing.JTextField txtciudad;
    private javax.swing.JTextField txtciudad1;
    private javax.swing.JTextField txtcodigo1;
    private javax.swing.JTextField txtcodigoMedicina;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtdireccion1;
    private javax.swing.JTextField txtespecialidad;
    private javax.swing.JTextField txtfecha1;
    private javax.swing.JTextField txtidmedico;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txtnombre1;
    private javax.swing.JTextField txtsexo;
    private javax.swing.JTextField txtsexo1;
    private javax.swing.JTextField txtsocial1;
    private javax.swing.JTable txttabla3;
    private javax.swing.JTextField txttel1;
    // End of variables declaration//GEN-END:variables
}
